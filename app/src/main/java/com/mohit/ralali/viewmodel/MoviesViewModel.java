package com.mohit.ralali.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.mohit.ralali.model.MovieDataSource;
import com.mohit.ralali.model.data.MovieDataModel;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MoviesViewModel extends ViewModel {

    public MutableLiveData<List<MovieDataModel>> movieMutableLiveData;


    public LiveData<List<MovieDataModel>> getImages() {

        if (movieMutableLiveData == null) {
            movieMutableLiveData = new MutableLiveData<>();
            loadImages();
        }

        return movieMutableLiveData;
    }

    private void loadImages() {
        MovieDataSource.getMovieList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(imageDataModels ->
                        movieMutableLiveData.setValue(imageDataModels));
    }
}
