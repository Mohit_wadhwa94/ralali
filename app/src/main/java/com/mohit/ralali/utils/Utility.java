package com.mohit.ralali.utils;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class Utility {

    private static final String TAG = Utility.class.getName();

    private static Context applicationContext;

    public synchronized static void initUtility(Context context) {
        applicationContext = context;
    }

    public static Context GetApplicationContext() {
        return applicationContext;
    }

    public static boolean isStringValid(String stringToValidate) {
        String trimmedStringToValidate = null;

        if (stringToValidate != null) {
            trimmedStringToValidate = stringToValidate.trim();
            return (!trimmedStringToValidate.isEmpty()
                    && !trimmedStringToValidate.equalsIgnoreCase("null")
                    && !trimmedStringToValidate.equalsIgnoreCase("None")
                    && !trimmedStringToValidate.equalsIgnoreCase("na"));
        }

        return false;
    }

    public static void setText(TextView textView, String text) {
        if (isStringValid(text)) {
            textView.setVisibility(View.VISIBLE);
            textView.setText(text);
        } else {
            textView.setVisibility(View.GONE);
        }
    }

    public static int getActivityWidth(Activity activity) {
        if (activity == null) {
            Log.e(TAG, "Activity sent to function GetScreenWith() is null");
            return -1;
        }

        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }

}
