package com.mohit.ralali.view;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mohit.ralali.R;
import com.mohit.ralali.model.data.MovieDataModel;
import com.mohit.ralali.utils.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.movie_name)
    TextView movieName;

    @BindView(R.id.director)
    TextView director;

    @BindView(R.id.release_data)
    TextView releaseDate;

    @BindView(R.id.language)
    TextView language;

    Activity activity;

    public MovieViewHolder(Activity activity, @NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

        this.activity = activity;
    }

    public void setData(MovieDataModel data) {
        if (data == null) {
            return;
        }

        String movieName = data.getTitle();
        String director = data.getDirector();
        String releaseData = data.getReleased();
        String language = data.getLanguage();

        Utility.setText(this.movieName, movieName);
        Utility.setText(this.director, director);
        Utility.setText(this.releaseDate, releaseData);
        Utility.setText(this.language, language);
    }
}
