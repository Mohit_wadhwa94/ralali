package com.mohit.ralali.view;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mohit.ralali.R;
import com.mohit.ralali.model.data.MovieDataModel;

import java.util.ArrayList;
import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter {

    public List<MovieDataModel> movieList = new ArrayList<>();
    private Activity activity;

    public MoviesAdapter(Activity activity) {
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_view_holder, parent, false);
        return new MovieViewHolder(activity, view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MovieViewHolder) {
            ((MovieViewHolder) holder).setData(movieList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public void addMovies(List<MovieDataModel> moviesDatModelList) {
        this.movieList.addAll(moviesDatModelList);
        notifyDataSetChanged();
    }
}
