package com.mohit.ralali.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mohit.ralali.R;
import com.mohit.ralali.model.data.MovieDataModel;
import com.mohit.ralali.viewmodel.MoviesViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieListActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    private MoviesViewModel imageViewModel;
    MoviesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        init();
    }

    private void init() {
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        imageViewModel = ViewModelProviders.of(this).get(MoviesViewModel.class);
        adapter = new MoviesAdapter(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }


    @Override
    protected void onResume() {
        super.onResume();
        imageViewModel.getImages().observe(this, this::setData);
    }

    private void setData(List<MovieDataModel> moviesDatModelList) {
        if (moviesDatModelList == null  || moviesDatModelList.size() < 1) {
            return;
        }

        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        if (adapter.movieList.size() < 1) {
            adapter.addMovies(moviesDatModelList);
        }
    }
}
