package com.mohit.ralali.init;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

public abstract class ApplicationManager extends Application {
	
	protected static final String TAG = ApplicationManager.class.getSimpleName();
	
	private static Context context;
	
	private static ApplicationManager mInstance;
	
	public void onCreate() {
		super.onCreate();
		mInstance = this;
	
		// Initialize the app context
		context = getApplicationContext();
	}

	public static synchronized ApplicationManager getInstance() {
		return mInstance;
	}
	
	public static Context getAppContext() {
		return context;
	}

	@Deprecated
	public static void setAppContext(Context cxt) {
		context = cxt;
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}
}
