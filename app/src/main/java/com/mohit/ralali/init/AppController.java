package com.mohit.ralali.init;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.StrictMode;

import com.mohit.ralali.utils.Utility;

public class AppController extends ApplicationManager {


    public static final String TAG = "AppController";

    @SuppressLint("StaticFieldLeak")
    private static AppController instance;

    public static AppController getInstance() {
        if (instance == null)
            throw new IllegalStateException();
        return instance;
    }

    public AppController() {
        instance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

        Utility.initUtility(getApplicationContext());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}