package com.mohit.ralali.model;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mohit.ralali.init.AppController;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static final String BASE_URL = " http://www.omdbapi.com/?apikey=fcb30dcc";

    private static Retrofit retrofit = null;

    public static Gson gson = new GsonBuilder()
            .create();

    private static String TAG = ApiClient.class.getName();

    private static Retrofit getClient() {
        if (retrofit == null) {
            OkHttpClient client = getHttpClient();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build();
        }

        return retrofit;
    }

    private static OkHttpClient getHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS);

        try {
            int cache_size = 10 * 1024 * 1024;
            File cacheFile = new File(AppController.getAppContext().getCacheDir(), "okHttpCache");
            Cache cache = new Cache(cacheFile, cache_size);
            builder.cache(cache);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage() + " at line " + 28);
        }

        return builder.build();
    }

    public static <T> T getService(Class<T> service) {
        Retrofit client = getClient();
        return client.create(service);
    }
}