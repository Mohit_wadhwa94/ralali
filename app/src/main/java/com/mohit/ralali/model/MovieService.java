package com.mohit.ralali.model;

import com.mohit.ralali.model.data.MovieDataModel;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface MovieService {

    @GET
    Observable<List<MovieDataModel>> getMovieList(@Query("y") String year,
                                                  @Query("type") String type,
                                                  @Query("s") String search);
}
