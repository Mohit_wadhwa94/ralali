package com.mohit.ralali.model;

import com.mohit.ralali.model.data.MovieDataModel;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class MovieDataSource {

    private static MovieService movieService;

    private static synchronized MovieService getMovieService() {
        if (movieService == null) {
            movieService = ApiClient.getService(MovieService.class);
        }

        return movieService;
    }

    public static Observable<List<MovieDataModel>> getMovieList() {

        //There is no direct way to fetch all the movies released in a particular year,
        // have hardcoded the data below to simulate the flow.

        MovieDataModel movieDataModel = new MovieDataModel();
        movieDataModel.setTitle("Once Upon a Time... in Hollywood");
        movieDataModel.setDirector("Quentin Tarantino");
        movieDataModel.setReleased("26 Jul 2019");
        movieDataModel.setLanguage("English, Italian, Spanish");

        MovieDataModel movieDataModel1 = new MovieDataModel();
        movieDataModel1.setTitle("The Diary of Diana B");
        movieDataModel1.setDirector("Dana Budisavljevic");
        movieDataModel1.setReleased("18 Jul 2019");
        movieDataModel1.setLanguage("Croatian, German");

        MovieDataModel movieDataModel2 = new MovieDataModel();
        movieDataModel2.setTitle("Plan C");
        movieDataModel2.setDirector("Alexia Dalla Rosa, Madison Graves, Justin Sudar");
        movieDataModel2.setReleased("30 Apr 2019");
        movieDataModel2.setLanguage("English");

        MovieDataModel movieDataModel3 = new MovieDataModel();
        movieDataModel3.setTitle("D/O Parvathamma");
        movieDataModel3.setDirector("Shankar");
        movieDataModel3.setReleased("24 May 2019");
        movieDataModel3.setLanguage("Kannada");

        List<MovieDataModel> movieDataModels = new ArrayList<>();
        movieDataModels.add(movieDataModel);
        movieDataModels.add(movieDataModel1);
        movieDataModels.add(movieDataModel2);
        movieDataModels.add(movieDataModel3);

        return Observable.from(movieDataModels)
                .map(movieList -> movieDataModels);
    }
}
