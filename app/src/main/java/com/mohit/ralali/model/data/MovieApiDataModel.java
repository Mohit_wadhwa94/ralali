package com.mohit.ralali.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class MovieApiDataModel implements Parcelable {

    private List<MovieDataModel> movieDataModels;

    public List<MovieDataModel> getMovieDataModels() {
        return movieDataModels;
    }

    public void setMovieDataModels(List<MovieDataModel> movieDataModels) {
        this.movieDataModels = movieDataModels;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.movieDataModels);
    }

    public MovieApiDataModel() {
    }

    protected MovieApiDataModel(Parcel in) {
        this.movieDataModels = new ArrayList<MovieDataModel>();
        in.readList(this.movieDataModels, MovieDataModel.class.getClassLoader());
    }

    public static final Parcelable.Creator<MovieApiDataModel> CREATOR = new Parcelable.Creator<MovieApiDataModel>() {
        @Override
        public MovieApiDataModel createFromParcel(Parcel source) {
            return new MovieApiDataModel(source);
        }

        @Override
        public MovieApiDataModel[] newArray(int size) {
            return new MovieApiDataModel[size];
        }
    };
}
